/**
 * @file board.h
 * @brief Defines the representation and utility functions related to the chess
 * board.
 */

#pragma once

/**
 * @brief The representation of the chess board.
 */
struct board {};