#include <string>

/**
 * @brief Logs a message to the console with information about the file and
 * location where it was called from.
 * @param message The message to log.
 * @param file The file where the message was called from.
 * @param line The line where the message was called from.
 * @return void
 */
void log(const char* message, const std::string file, int line,
         const std::string level = "DEBUG");