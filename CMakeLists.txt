cmake_minimum_required(VERSION 3.27)
project(betaone
  DESCRIPTION "A chess engine in C++"
  LANGUAGES C CXX
  VERSION 0.1.0
)

set(CMAKE_CXX_STANDARD 17)

file(GLOB SOURCES "src/*.cpp")
file(GLOB HEADERS "includes/*.h")

add_executable(betaone ${SOURCES} ${HEADERS})
