#include "../includes/log.h"

/**
 * @brief The entry point of the program.
 * @return 0 if the program exited successfully.
 */
int main() {
  log("starting betaone chess engine v0.0.1", __FILE__, __LINE__, "INFO");
  return 0;
}