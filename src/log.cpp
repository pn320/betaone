#include <iostream>
#include <string>
#include <map>
#include "../includes/log.h"

/**
 * @brief A map of log levels to their respective colors.
 * @return A map of log levels to their respective colors.
 */
const std::map<std::string, std::string> log_levels{{"DEBUG", "\x1B[32m"},
                                                    {"INFO", "\x1B[32m"},
                                                    {"WARN", "\x1B[33m"},
                                                    {"ERROR", "\x1B[31m"},
                                                    {"FATAL", "\x1B[31m"}};

/**
 * @brief Returns the color string associated with a log level.
 * @param level The log level to get the color for.
 * @return The color associated with the log level.
 */
std::string get_log_level(const std::string level) {
  return log_levels.at(level);
}

void log(const char* message, const std::string file, int line,
         const std::string level) {
  std::string log_location = "[" + get_log_level(level) +
                             file.substr(file.find_last_of("/") + 1) + "@" +
                             std::to_string(line) + "\033[0m]:";
  std::cout << log_location << " " << message << std::endl;
}